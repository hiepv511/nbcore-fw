<?php
/**
 * The plugin bootstrap file
 *
 *
 * @link              netbaseteam.com
 * @since             1.0.0
 * @package           Nb_Fw
 *
 * @wordpress-plugin
 * Plugin Name:       Netbase Framework
 * Plugin URI:        netbaseteam.com
 * Description:       Plugin that hold almost all functionality of Core theme
 * Version:           1.0.0
 * Author:            netbaseteam
 * Author URI:        netbaseteam.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       nb-fw
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

function activate_nb_fw() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-nb-fw-activator.php';
	Nb_Fw_Activator::activate();
}

function deactivate_nb_fw() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-nb-fw-deactivator.php';
	Nb_Fw_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_nb_fw' );
register_deactivation_hook( __FILE__, 'deactivate_nb_fw' );

require plugin_dir_path( __FILE__ ) . 'includes/class-nb-fw.php';

//TODO Fix this: Class vendor folder
require plugin_dir_path( __FILE__ ) . 'vendor/metaboxes/metaboxes.php';

function run_nb_fw() {

	$plugin = new Nb_Fw();
	$plugin->run();

}
run_nb_fw();
