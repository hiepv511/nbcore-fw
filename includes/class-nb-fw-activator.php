<?php

/**
 * Fired during plugin activation
 *
 * @link       netbaseteam.com
 * @since      1.0.0
 *
 * @package    Nb_Fw
 * @subpackage Nb_Fw/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Nb_Fw
 * @subpackage Nb_Fw/includes
 * @author     netbaseteam <hiepv511@gmail.com>
 */
class Nb_Fw_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
